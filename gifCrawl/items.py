# -*- coding: utf-8 -*-

# Author: milkjug (Markia Theus)
# Created August 7th, 2016.

import scrapy


class GifcrawlItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    gif_urls = scrapy.Field()
    pass
