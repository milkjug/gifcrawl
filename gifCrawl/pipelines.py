# -*- coding: utf-8 -*-

# Author: milkjug (Markia Theus)
# Created August 7th, 2016.


import scrapy
from scrapy.contrib.pipeline.images import ImagesPipeline

class ImgurPipeline(ImagesPipeline):

    def check_gif(self, image):

    	if image.format == 'GIF':
        return True


def persist_gif(self, key, data, info):

        root, ext = os.path.splitext(key)
        key = key + '.gif'
        absolute_path = self.store._get_filesystem_path(key)
        self.store._mkdir(os.path.dirname(absolute_path), info)
        f = open(absolute_path, 'wb')  
        f.write(data)

  def image_downloaded(self, response, request, info):
        checksum = None
        for key, image, buf in self.get_images(response, request, info):
            if checksum is None:
                buf.seek(0)
                checksum = md5sum(buf)
            if self.check_gif(image):
            	# key.startswith('full')  and?

                # If no error, then save the gif
                self.persist_gif(key, response.body, info)
            else:
                self.store.persist_image(key, image, buf, info)
        return checksum