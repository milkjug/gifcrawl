# -*- coding: utf-8 -*-

# Author: milkjug (Markia Theus)
# Created August 7th, 2016.


import scrapy


class GifcrawlerSpider(Crawl.Spider):
    name = "gifcrawler"
    allowed_domains = ["www.giphy.com, www.gifs.com"]
    start_urls = ['http://giphy.com/', 'http://gifs.com/']


  def parse_item(self, response):
    self.log('Looking for Gifs @ %s' % response.url)
    sel = Selector(response)
    images = sel.xpath('//saved-                                                                                t')
    items = []
    for image in images:
      item = Image()
      item['parent_page'] = response.url
      item['src'] = image.select('@src').extract()
      items.append(item)

    return items