# -*- coding: utf-8 -*-
# Author: milkjug (Markia Theus)
# Created August 7th, 2016

BOT_NAME = 'gifCrawl'

SPIDER_MODULES = ['gifCrawl.spiders']
NEWSPIDER_MODULE = 'gifCrawl.spiders'
ITEM_PIPELINES = {'gifCrawl.pipelines.gifCrawlPipeline' :1}
IMAGES_STORE = '~/Desktop/gifCrawl-results'

ROBOTSTXT_OBEY = True
