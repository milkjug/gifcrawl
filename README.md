![alt text](http://i.imgur.com/Hcz5cRd.png "gifCrawl Logo")

### gifCrawl

A simple web crawler spider for gif images on certain websites such as Giphy.com, Gifs.com or Gifs.gif

Implemented through Python and Scrapy framework


Install Scrapy: 

`sudo pip install scrapy` 

Modify 

`gifCrawl/gifCrawl/spiders/gifcrawler.py`

to select the websites you want to crawl for gifs.


[Read Scrapy Docs](https://scrapy.org/doc/)
